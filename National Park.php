<!DOCTYPE html>
<html lang="en">
<head>
  <title>National Park</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular National Parks</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Kanger_Ghati_National_Park" target="_blank">
          <img src="img/p1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Kanger Ghati National Park</center></h3>

            <p>Kanger Ghati National Park (also called Kanger Valley National Park) was declared a national park in 1982 by the Government of India. Among the various protected areas in the country, the Kanger Valley National Park near Jagdalpur, in the Bastar region of Chhattisgarh is one of the densest national parks, well known for its biodiversity, landscape, waterfalls, subterranean geomorphologic limestone caves, and home for the Bastar hill myna, the state bird of Chhattisgarh.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Indravati_National_Park" target="_blank">
          <img src="img/p2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Indravati National Park</center></h3>
            <p>Indravati National Park is a national park located in Bijapur district of Chhattisgarh state of India. It derives its name from the nearby Indravati River. It is home to one of the last populations of rare wild buffalo. Indravati National Park is the finest and most famous wildlife parks of Chhattisgarh. It is one among the three project tiger sites in Chhattisgarh along with Udanti-sitanadi, Indravati National Park is located in Bijapur district of Chhattisgarh. The park derives its name from the Indravati River, which flows from east to west and forms the northern boundary of the reserve with the Indian state of Maharashtra.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Sanjay_National_Park" target="_blank">
          <img src="img/p3.jpg" alt="Fjords" style="width:100%" "height:100%">
          <div class="caption">
             <h3><center> Guru Ghasidas National Park </center></h3>
            <p>The Guru Ghasidas National Park is located in Chhattisgarh and in the Koriya, Sidhi and Singrauli districts of Madhya Pradesh state, India. It is a part of the Sanjay-Dubri Tiger Reserve system. The park covers 466.7 km2. It is located in the Narmada Valley dry deciduous forests' ecoregion. In June 2011, the proposal to declare Guru Ghasidas National Park as a tiger reserve was proposed by the then Minister of State (Independent Charge) for Environment and Forests Jairam Ramesh to Chhattisgarh Chief Minister Raman Singh.</p>
          </div>
        </a>
      </div>
    </div>
 

</div>

</body>
</html>


