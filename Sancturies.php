<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sancturies</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular Sancturies</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Achanakmar_Wildlife_Sanctuary" target="_blank">
          <img src="img/sa1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Achanakmar </center></h3>

            <p>The Achanakmar Wildlife Sanctuary is an Indian sanctuary in Mungeli district, Chhattisgarh State. It had been established in 1975, under the provisions of the Indian Wildlife Protection Act of 1972, and declared a Tiger Reserve under Project Tiger, in 2009. It is a part of the Achanakmar-Amarkantak Biosphere Reserve.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.chhattisgarhgyan.in/2018/11/bhairamgarh-wildlife-santurary.html" target="_blank">
          <img src="img/sa2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Bhairamgarh</center></h3>
            <p>Bhairamgarh Wildlife Sanctuary (BWS) is geographically situated on the east of Bhairamgarh in Bijapur. It is approximately 50 kms. away from Bijapur. The history of BWS dates back to the year 1983. The Government of Chhattisgarh established it in the year 1983 to protect the three remaining groups of wild buffaloes along with different species of plants, trees, birds and wild animals. BWS is covered by marshes,bushes and water bodies. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/barnawapara-wildlife-sanctuary-chhattisgarh.html" target="_blank">
          <img src="img/sa3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Barnawapara </center></h3>
            <p>Barnawapara Wildlife sanctuary, is named after Bar and Nawapara forest villages, which are in the heart of the sanctuary. It is a land mass of undulating terrain dotted with numerous low and high hillocks well forested area of North-eastern corner of Raipur district. The Tributaries of Mahanadi are the source of water. River Balamdehi forms the western boundary and Jonk River forms the north-eastern boundary of the Sanctuary.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://raigarh.gov.in/en/tourist-place/gomarda-wildlife-sanctuary/" target="_blank">
          <img src="img/sa4.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Gomarda</center></h3>
            <p>Gomarda Wildlife Sanctuary is situated near Sarangarh. There are a lot of hilly terrains in the region. Spread over 275 sq km it is one of the ideal destination for the adventure seekers. Gomarda Wildlife Sanctuary, Chattisgarh has different varities of wild animals. Some of the common species are different types of dears and buffalos. Tourists visit the place to see wild buffalos who move in flocks and make for a pretty sight.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.chhattisgarhgyan.in/2018/11/pamed-wildlife-sanctuary.html" target="_blank">
          <img src="img/sa5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Pamed  </center></h3>
            <p>Spread over an area of 262 sq km, Pamed is one of the most important wildlife sanctuaries in Bastar. Established in 1983 to accommodate the excess population of the wild buffalo, this sanctuary is also home to the tiger, leopard, chital and various other species of fauna.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://hi.wikipedia.org/wiki/%E0%A4%B8%E0%A5%87%E0%A4%AE%E0%A4%B0%E0%A4%B8%E0%A5%8B%E0%A4%A4_%E0%A4%85%E0%A4%AD%E0%A4%AF%E0%A4%BE%E0%A4%B0%E0%A4%A3%E0%A5%8D%E0%A4%AF" target="_blank">
          <img src="img/sa6.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Semarsot </center></h3>
            <p>Semarsot Wildlife Sanctuary is situated at a distance of 50 km from Ambikapur, the headquarter of Surguja district. Located on the Ambikapur- Daltongunj road, it sprawls over an area of 430.36 sq km. Jainagar is the closest railway station while Varanasi is the nearest airport at a distance of 31 km.


          </div>
        </a>
      </div>
    </div>
  </div>
     <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Sitanadi_Wildlife_Sanctuary" target="_blank">
          <img src="img/sa7.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Sitanadi</center></h3>
            <p>Sitanadi Wildlife Sanctuary is located in Dhamtari District, Chhattisgarh, India. Sitanadi Wildlife Sanctuary is a famous tourist attraction which is frequented by wildlife enthusiasts throughout the year. The wildlife sanctuary was established in 1974 under Wildlife Protection Act of 1972. This sanctuary sprawls over an area of 556 km2 and has an altitude ranging between 327 and 736 m above the sea level. It is named after Sitanadi River which originates from this sanctuary and joins Mahanadi River near Deokhut. Sitanadi Wildlife Sanctuary is known for its lush green flora and rich and unique and diverse fauna and has great potential to emerge as one of the finest wildlife destinations in central India.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://indiasendangered.com/eco-travel-udanti-wildlife-sanctuary-chhatisgarh/" target="_blank">
          <img src="img/sa8.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Udanti</center></h3>
            <p>The park is located between 20.45-20.79ºN latitudes and 81.98-82.09ºE longitudes. It is spread over an area of 247 sq.km. The sanctuary is located 160km away from Raipur, the state’s capital and a major transportation hub. Udanti was notified as a wildlife sanctuary in 1983. Together with the 556 sq.km Sitanadi wildlife sanctuary, it constitutes the Udanti-Sitanadi Tiger Reserve, which was notified in 2008 as Chhattisgarh’s 3rd tiger reserve. The wild buffalo population had declined to a mere 7 individuals in 2007, when the Wildlife Trust of India initiated a conservation programme for the sanctuary’s flagship species. The population has increased to 13 since. Some of the buffaloes are kept in a large enclosure, to avoid poaching and competition from livestock.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Tamor_Pingla_Wildlife_Sanctuary" target="_blank">
          <img src="img/sa9.webp" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Tamor Pingla  </center></h3>
            <p>The northern boundary is the Moran river, eastern boundary is Bonga Nalla, and western boundary is Rihand River. This was notified as Wildlife Sanctuary in 1978. In 2011, it was notified by Chhattisgarh's Government as a part of Surguja Jashpur Elephant Reserve. There are seven revenue villages within this sanctuary, namely Khond, Injani, Archoka, Durgain, Kesar, Chattauli and Dhaulpur. Except for Khond, these villages are very small, with less than 20 households. The Tamor Hills, having an area of 250 km2, is a table land rising sharply from the neighboring villages of Tamki, Ghui and Barpetia. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://kawardha.gov.in/tourist-place/bhoramdev-centrury/" target="_blank">
          <img src="img/sa10.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Bhoramdev</center></h3>
            <p>Bhoramdev Sanctuary is spread over Green Hariers. Expanded in the area of 352 square kilometer, this sanctuary boasts many natural and prehistoric features in it. Kanha National Park and Achanakmar Tiger reserve provides protection for both. Bhormadev Sanctuary was notified in the year 2001. Then Chhattisgarh was formed as a new state. Along with the Chilphi Valley, an extended buffer zone of Kanha National Park also came to the newly formed Chhattisgarh state.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://raipur.gov.in/tourist-place/jungle-safari/" target="_blank">
          <img src="img/sa12.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Nandanvan Jungle Safari</center></h3>
            <p>The Jungle Safari, located at sector-39, Naya Raipur. Naya Raipur is about 35km from Raipur Railway Station and 15km from the Swami Vivekanand Airport, Raipur. The entire 800 Acre area of the Nandanvan Jungle Safari is lush green, with beautiful landscapes. Several indigenous plant species also add up to the vegetation, creating a natural habitat for the animals. It has 130 Acre of water body named ‘Khandwa Reservoir’ which attracts many migratory bird species. Four safaris namely, herbivore, Bear, tiger & Lion Safari has been established. Another 32 species of animal will be exhibited in the upcoming zoo.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://bilaspur.gov.in/en/tourist-place/kanan-pendari/" target="_blank">
          <img src="img/sa13.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Kanan Pendari</center></h3>
            <p>
Bilaspur city is famous for Kanan Pendari Zoo. It is a small zoo located near Sakri, about 10 km from Bilaspur on Mungali Road.
City bus is operated by Bilaspur City Bus Ltd for transportation of commuters.It's a zoo with lots of animals , birds , reptiles and fishes many other things to explore you can spend a half day tour with us and also you can visit wonderworld on the same day. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Maitri_Bagh" target="_blank">
          <img src="img/sa14.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Maitri Bagh</center></h3>
            <p>The Maitri Bagh Zoo (mini zoo park) is located in the Bhilai, India. It Is the biggest zoo in the region. The zoo comprises 111 acres (44.94 ha) of park lands. It is run by the Public Sector Undertaking, Steel Authority of India Limited. Maitri Bagh is a "Friendship Garden" established as symbol of India USSR friendship. It was developed and maintained by Bhilai Steel Plant, SAIL.It was established in the year 1972.</p>
          </div>
        </a>
      </div>
    </div>
    
  </div>
  
  

</div>

</body>
</html>


