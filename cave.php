<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cave</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular Cave</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Kailash </center></h3>

            <p>Buried deep in the forest close to the Tirathgarh waterfalls, this underground cave, about 40 km from Jagdalpur, has the most spectacular formations of stalactites (limestone pillars hanging down from the roof) and stalagmites (pillars rising from the ground). Millions of years old, it is deep in a hill, 200 meters long, 35 meters wide and 55 meters deep. If you remember that stalactites and stalagmites are formed drop by drop, and that an inch takes about 6,000 years to form, the huge pillars of the Kanger Caves will leave you speechless. Some of the stalagmites have markings, indicating that they have been worshipped as Shiva Lingams.  </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c2.webp" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Kutumsar </center></h3>
            <p>As you enter, you realize what the phrase “pitch-dark” means. As your guide holds up a lamp, the stalactites and stalagmites come alive as mystic creations of a master sculptor. Bastar are sure to mesmerize you with their charisma. Kutumsar Caves situated in the Kanger Valley National Park, stand at 38 km from Jagdalpur. The Kutumsar Caves display magnificent formations of stalactites and stalagmites and are probably the darkest of their sort, 35 meters below the ground level. The mineral rich rocks of this area have, over time, added bands of brilliant color to these rock formations. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Singhanpur</center></h3>
            <p>Singhanpur is most popular for its ancient caves.. Singhanpur is situated 20 kms. away from Raigarh and has the distinction of hosting the oldest sculptures on earth. These sculptures are claimed to have been created in 30000 B.C. and look very similar to Mexican and Spanish sculptures.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c4.png" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Sita Bhagara</center></h3>
            <p>The most mythological and historically important tourist destination of Ramgarh is the Sita Bengra Cave, or the residence of Goddess Sita. Located on a north-eastern slope of the hillock, the Sita Bengra Cave is 14m long, 4.2m bread with a height of 2m in the front which lessens on the back side. Outside the cave there are many circular mates and benches carved out of ancient stone. Human footprints in the right comer add to the mystical aura and stand testimony to the fact that Sitaji lived in the cave during her forest stay.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Jogimara </center></h3>
            <p>Jogimara Caves are located in the Surguja District in the state of Chhattisgarh. These caves are around 10x6x6 feet in dimensions and age back to 300 BC. There are many paintings of animals, human beings, birds and flowers on these caves. Each painting is painted on the white base plaster with a red outline.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c6.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Ramgarh</center></h3>
            <p>There are many caves in Ramgarh. It is believed to be the most ancient theater in the world. It is also believed that lord Rama has come here and lived during his exile. This is the theater of Kalidas (one of the biggest poet in world of Sanskrit). Here, if someone speaks in the theater his/her sound can be heard far away. In another cave there are some paintings of ancient time. Also there is a long hole, it is believed that when lord Ram lived here this hole was used to keep a watch on invaders. It is also believed that Ramgarh lies on the way to Lanka, the Ravan (The demon who kidnapped Sita) took Sita (wife of lord Ram) from.</p>
          </div>
        </a>
      </div>
    </div>
   
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.chhattisgarhtourism.co.in/caves-of-chhattisgarh.html" target="_blank">
          <img src="img/c7.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center> Aranyak </center></h3>
            <p>The Aranyak cave is situated near the village called as Matherkanta in Mangalgiri Hills located in the district of Bastar. The hill is very beautifully decorated with greenery and lush around. The Mangalgiri Hills is located approx 50kms from Jagdalpur. The cave is a pure discovery by the people around. The cave is huge enough with a length of 179 feet. The cave is accidently discovered by some tourists in the year 1996 passing through the dense forest.</p>
          </div>
        </a>
      </div>
    </div>
  
  

</div>

</body>
</html>


