<!DOCTYPE html>
<html lang="en">
<head>
  <title>Hill</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular Hills</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayrentals.co.in/articles/details/58/5-Best-Hill-Stations-in-Chhattisgarh.html" target="_blank">
          <img src="img/h1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Mainpat</center></h3>

            <p>Mainpat is a small village in the Surguja district of the State. This is a popular hill station of Chhattisgarh perched at a height of 1100 m above sealevel. The place is locally known as ‘Mini Tibet’ as the majority of the people residing here are Tibetan refugees. This small village is also nicknamed as 'Shimla/Swiss of Chhattisgarh’ because of its lush greenery and natural beauty. This is the best honeymoon destination of the state. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayrentals.co.in/articles/details/58/5-Best-Hill-Stations-in-Chhattisgarh.html" target="_blank">
          <img src="img/h2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Chirmiri</center></h3>
            <p>Chirmiri is a beautiful hill station in the Koriya district of the State. The hill station is also referred as the ‘Jannat/Heaven of Chhattisgarh’. It has the credit of being the second biggest crane in the Asian continent. The place is located at a height of about 579m above sea level. It is surrounded by lush greenery, temples, and coal mines. It has a pleasant climate throughout the year. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayrentals.co.in/articles/details/58/5-Best-Hill-Stations-in-Chhattisgarh.html" target="_blank">
          <img src="img/h3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Baila Dila</center></h3>
            <p>Baila Dila hills are one of the most attractive hill stations of the State. The hill station is located at an elevation of 1,276m above sea level. The place is located in the Dantewada district of the State. The hill station is blessed with enormous natural beauty such as scenic waterfalls, gardens and forests. These mountains are having rich deposits of iron ore. They are separated into two zones, namely the Bacheli and the Kirandul.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayrentals.co.in/articles/details/58/5-Best-Hill-Stations-in-Chhattisgarh.html" target="_blank">
          <img src="img/h4.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Gadiya</center></h3>
            <p>Gadiya Mountain is the highest mountain located in the Kanker district of the State. This mountain is also known as the Kila Dongri. The main tourist attraction here is the famous Doodh River flowing beneath the mountain. Mahashivaratri is the main festival celebrated here. Thousands of tourists and pilgrims come here for offering prayers. It is believed that there exists a water tank on the top of the mountain that never runs dry.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayrentals.co.in/articles/details/58/5-Best-Hill-Stations-in-Chhattisgarh.html" target="_blank">
          <img src="img/h5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Ambikapur hill station </center></h3>
            <p>Ambikapur hill station is located in the Surguja district of the State. The place is located at an elevation of 623m above sea level. The majority of the terrain in the region is full of dense forests and hills. This is a perfect holiday destination with temples, caves, hills, rivers, hot water springs and spectacular waterfalls. The main tourist attractions of the hill station are Ramgarh hill, Deogarh, Thin-Thini Patthar, Pawai Water Fall, Tamor Pingla Wildlife Sanctuary and the Semarsot Wildlife Sanctuary.</p>
          </div>
        </a>
      </div>
    </div>
   
  </div>
  
  

</div>

</body>
</html>


