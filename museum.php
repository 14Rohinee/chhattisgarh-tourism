<!DOCTYPE html>
<html lang="en">
<head>
  <title>Museum</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular Museum</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://cgculture.in/museum_front_detail.aspx?id=UHrN1qKEnlRZH5Ff9B3JBg==" target="_blank">
          <img src="img/m1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>District Archaeological Museum </center></h3>

            <p>The antiquities received from the Bilaspur district have been increasingly attracting the various museums of the country, mainly the materials obtained from the zonal are collected in the Nagpur, Raipur, Jabalpur and Sagar University museums. Prior to the local collection, antiquities were collected in the district headquarters residence according to tradition. Important role of museums is unquestionable in terms of developing interest in public-general towards ancient cultural heritage, providing their necessary support for awareness, awareness and research.   </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://cgculture.in/museum_front_detail.aspx?id=0KiUQxeX+AEch5QDfaUNnA==" target="_blank">
          <img src="img/m2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Mahant Ghasidas Memorial Museum</center></h3>
            <p>
In 1875, the first of Chhattisgarh and one of the first ten museums of the country started with the donation of  Mahant Ghasidas  of  Rajnandgaon. The Raipur Museum was initially run by the Municipality and the District Council. This museum was first established in the octagonal building east of the present ministry premises. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidify.com/places/jagdalpur/anthropological-museum-sightseeing-1254531.html" target="_blank">
          <img src="img/m3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Zonal Anthropological museum</center></h3>
            <p>The Zonal Anthropological museum was established in Jagdalpur in 1972, to provide an insight into the culture and lifestyle of the Bastar tribe. The museum has a brilliant and well-curated collection of ethnographic items that shed light on the life of Bastar tribes. Artworks depicting their daily life, sculptures, a variety of objects like clothes, footwear, headgear, ornaments, wood carvings, weapons, utensils and masks among many others can be seen here. It is an excellent window to gain an understanding of the lifestyle of ‘Adivasis’ of Bastar. Whether or not you’re a history buff, this museum is sure to raise your curiosity into the life and culture of tribals.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.hellotravel.com/india/mahakoshal-art-gallery" target="_blank">
          <img src="img/m4.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Mahakoshal Art Gallery</center></h3>
            <p>Mahakoshal Art Gallery is located in Raipur. Make Mahakoshal Art Gallery a centerpiece of Raipur holiday itinerary. It is an area to examine drawing, portray and such art work. This could be very thrilling region for artwork and drawing enthusiasts. Rare heritage assets is this Mahakoshal artwork Gallery. This art gallery showcases all the artwork of the area people, the building also hosts some of art exhibitions.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.inspirock.com/india/bhilai/nehru-art-gallery-a4335836819" target="_blank">
          <img src="img/m5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Nehru Art Gallery </center></h3>
            <p>Nehru Art Gallery is located in Bhilai. Choose to start, finish, or center your holiday on a trip to Nehru Art Gallery by using our Bhilai trip itinerary maker.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://wikimapia.org/3974649/Purkhauti-Muktangan-Museum" target="_blank">
          <img src="img/m6.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Purkhauti Muktangan</center></h3>
            <p>This Meuseum is being developed by Government of Chhattisgarh, Culture Department under Ministry of Tourism. The habitate, artifacts, folk dances, food habits of the tribals are being desplayed. Medicinal and traditional plantation is being grown.</p>
          </div>
        </a>
      </div>
    </div>
   
  </div>
  
  
  

</div>

</body>
</html>


