<!DOCTYPE html>
<html lang="en">
<head>
  <title>Temple</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    .back{
  color: navy;
  padding-top: 3px;
  padding-left: 4px;
}
    
  </style>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
   <div class="back"><h3>
 <a href="index.html">
          &nbsp;<span class="glyphicon glyphicon-arrow-left "></span>
        </a></h3></div>

<div class="container">
  <h2><center>Most Popular Temples</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Bambleshwari_Temple" target="_blank">
          <img src="img/t1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Maa Bamleshwari Temple</center></h3>

            <p>Bambleshwari Temple is at Dongargarh in Rajnandgaon district, Chhattisgarh, India. It is on a hilltop of 1600 feet. This temple is referred as Badi Bambleshwari.Another temple at ground level, the Chhotti Bambleshwari is situated about 1/2 km from the main temple complex.There is tradition of lighting Jyoti Kalash during Navaratris here.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Bhoramdeo_Temple" target="_blank">
          <img src="img/t2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Bhoramdeo Temple</center></h3>
            <p>Bhoramdeo Temple is a complex of Hindu temples dedicated to the god Shiva in Bhoramdeo, in the Indian state of Chhattisgarh.It comprises a group of four temples of which the earliest is a brick-temple.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://hi.wikipedia.org/wiki/%E0%A4%AE%E0%A4%BE%E0%A4%81_%E0%A4%9A%E0%A4%82%E0%A4%A6%E0%A5%8D%E0%A4%B0%E0%A4%B9%E0%A4%BE%E0%A4%B8%E0%A4%BF%E0%A4%A8%E0%A5%80_%E0%A4%A6%E0%A5%87%E0%A4%B5%E0%A5%80_%E0%A4%AE%E0%A4%82%E0%A4%A6%E0%A4%BF%E0%A4%B0" target="_blank">
          <img src="img/t3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Chandrahasini Devi temple</center></h3>
            <p>Here Chandrahasini Devi temple is situated on the bank of Mahanadi. A very big fate is being organized here every year on the eve of Navratri.It is also famous as a tourist place. People from other state like Orissa are also visiting this place. Day by day it is becoming a big place for holy as well as tourist place.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Jagannath_Temple,_Puri" target="_blank">
          <img src="img/t4.png" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Jagannath Puri Temple</center></h3>
            <p>Jagannath Puri Temple Chennai is a Hindu temple dedicated to the divine trinity Jagannath, Baladeva and Subhadra in Chennai, India. The temple located in Kannathur off the East Coast Road by the seaside is built in Kalinga architecture reminiscent of the Jagannath Temple, Puri.[1] The temple has shrines dedicated to Shiva, Ganesh, Bimala. The annual Rathyatra is the main festival celebrated.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Mahamaya_Temple" target="_blank">
          <img src="img/t5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Mahamaya Temple </center></h3>
            <p>Mahamaya Temple is temple dedicated to dual Goddess Lakshmi & Saraswati, located in Ratanpur and is one of the 52 Shakti Peethas, shrines of Shakti, the divine feminine, spread across India.Ratanpur is a small city, full of temples and ponds, situated around 25 km from district Bilaspur of Chhattisgarh.Goddess Mahamaya is also known as Kosaleswari,presiding deity of old Daksin Kosal region(modern Chhattisgarh state).</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://hi.wikipedia.org/wiki/%E0%A4%B6%E0%A4%BF%E0%A4%B5%E0%A4%B0%E0%A5%80_%E0%A4%A8%E0%A4%BE%E0%A4%B0%E0%A4%BE%E0%A4%AF%E0%A4%A3" target="_blank">
          <img src="img/t6.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Shivrinarayan Temple </center></h3>
            <p>Who was Shabari? Did Lord Rama really eat berry fruits from Shabari at Shivrinarayan? Are the floating stones a reality or a myth? You may find some of the answers to these questions, when you visit Shivrinarayan, a small city located at the banks of River Mahanadi in Janjgir-Champa District of Chhattisgarh. This city is famous for a temple that was built some 2500 years back and is dedicated to Lord Rama and Shabari. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
     <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Danteshwari_Temple" target="_blank">
          <img src="img/t7.webp" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Danteshwari Temple</center></h3>
            <p>Danteshwari Temple is temple dedicated to Goddess Danteshwari, and is one of the 52 Shakti Peethas, shrines of Shakti, the divine feminine, spread across India. The temple built in the 14th century, is situated in Dantewada, a town situated 80 km from Jagdalpur Tehsil, Chhattisgarh. Dantewada is named after the Goddess Danteshwari, the presiding deity of the earlier Kakatiya rulers. Traditionally she is the Kuldevi (family goddess) of Bastar state.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.bhaskar.com/news/CHH-RAI-maa-mahamaya-temple-in-raipur-4760025-PHO.html" target="_blank">
          <img src="img/t8.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Mahamaya Temple, Raipur</center></h3>
            <p>Mahamaya Temple in Raipur is located in the old fort area of the city. This ancient 11th century temple lying on the banks of River Khaaroon belongs to the Kalchuri era, has been renovated many times and is visited by many devotees daily.October to March is the best time to visit Mahamaya Temple. Taxis and auto rickshaws are available to the Mahamaya temple from the Raipur airport and Raipur railway junction, which are located in the vicinity of the temple.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://gariaband.gov.in/en/tourist-place/ghatarani-temple/" target="_blank">
          <img src="img/t9.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Jatmai Ghatarani Temple </center></h3>
            <p>It is a big waterfall located 25km from Jatmai Temple. In Gatarani Temple Navratri festival is celebrated with much enthusiasm and devotion. Here we sees a decoration especially on festive occasions like Navratri. After the monsoon it is the best time for visit. Beautiful waterfall flows near the temple, which makes this place more attractive The waterfall in full flow to make the destination as a favorite picnic spot for the whole family.Waterfall is the best place to take a dip before entering the temple. The more adventurous can take a hike in the woods. Easily vehicles are available from Raipur to Ghatarani Temple. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidify.com/places/raipur/banjari-mata-mandir-sightseeing-1254224.html" target="_blank">
          <img src="img/t8.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center> Banjari Mata Mandir</center></h3>
            <p>The shrines of Banjara Mata Temple are, without a doubt, its speciality. Devoted to Goddess Banjari Mata, this sacred place is famous for hosting events during the Dussehra and Navratri festivals. Many devotees from neighbouring states and across the country flock to breathe in the pleasant and sacred aura. One can feel the calmness lingering in the air. The temple is open all days of the week and can be visited between 6:00 AM to 7:30 PM. The temple's exquisiteness and authenticity is the reason why thousands visit the place to seek blessings.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://rajnandgaon.nic.in/tourist-place/patal-bhairvi-temple/" target="_blank">
          <img src="img/t10.1.webp" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Patala Bhairavi Temple</center></h3>
            <p>Barfani dham is a temple in the town of Rajnandgaon in Chhattisgarh. A large Shiva Linga can be seen at the top of the temple while a large Nandi statue stands in front of it.The temple is constructed in three levels. The bottom layer is the shrine of Patal bhairavi, the second is the Navadurga or Tripura Sundari shrine and the upper level is of Shiva.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Dudhadhari_temple" target="_blank">
          <img src="img/t19.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Dudhadhari Math</center></h3>
            <p>
Sree Dudhdhari Math was founded by Sree Swami Balabhadradas Ji Maharaj, a monk belonging to 16th Century. As per legends Sree Swami Balabhadradas Ji Maharaj used to survive only on milk and hence came to be popularly known as Dudh Ahari Maharaj (one who consumes only milk). With the passage of time, for the sake of ease of pronunciation, the term evolved as Dudhdhari. The Math thus takes the name Dudhdhari Math. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.pilgrimaide.com/hatkeshwar-mahadev-temple,-raipur.html" target="_blank">
          <img src="img/t12.webp" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Hatkeshwar Temple</center></h3>
            <p>Located 5 kms from Raipur on the banks of River Kharun, Chhattisgarh, Hatkeshwar Mahadev Temple is one of the most sacred pilgrimage destinations in central India dedicated to Lord Shiva worshipped as Lord Hatakeshwar. The temple was built by Hajiraj Naik during the rule of Brahmadeo Rai, son of King Ramachandra of Kalchuri dynasty in 1402.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.apnisanskriti.com/temple/ganga-maiya-temple-jhalmala-balod-durg-chhattisgarh-4076" target="_blank">
          <img src="img/t13.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Ganga Maiya Temple</center></h3>
            <p>Ganga Maiya Temple is located at Jhalmala, near Balod-Durg road, in Balod Tehsil of Chattisgarh. It is a religious place with historic importance. This temple has a glorifying bliss and a very enchanting history. Originally, Ganga Maiya temple was constructed by a local fisherman in the form of a small hut. A local religious belief of Balod is related to the origin of Ganga Maiya Temple. Initially, the temple was built in form of a small hut. Several devotees donated a good amount of money which helped to construct it into a proper temple complex. Since it is located on the Balod – Durg road, it’s absolutely convenient to reach the shrine from any district of Chhattisgarh.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.indianholiday.com/tourist-attraction/kanker/holy-places-in-kanker/shivani-temple.html" target="_blank">
          <img src="img/t10.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Shivani Temple, Kanker</center></h3>
            <p>Shivani Temple, Kanker is also known as Shivani Maa Temple. It is believed that the Goddess in this temple is a combination of two Goddesses, namely Durga Maa and Kali Maa. The beautiful city of Kanker is a municipality in the Kanker District. Kanker District is located in the southern region of Chhattisgarh. The five rivers that flow through the district include Hatkul River, Mahanadi River, Turu River, Sindur River and Doodh River. The district's economy is based on agriculture. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://gariaband.gov.in/en/tourist-place/rajiv-lochan-temple/" target="_blank">
          <img src="img/t14.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center> Rajiv Lochan Temple</center></h3>
            <p>ocated in right bank of the Mahanadi river on the north-east of Gariaband, where it joins its tributaries called “Parry” and “Sodhur”. It is connected by road from the district headquarters and regular runs buses on the road. It is 45km far from Raipur district headquarter in the south-east. A small rail line “Raipur-Dhamtari” emanating from the Abhanapur and combines Navapara located on the other side of Rajim. Due to high bridge on the river near Rajim, road contacts has been established for twelve-months.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.patrika.com/raipur-news/raipur-maha-aarti-in-ram-mandir-1545954/" target="_blank">
          <img src="img/t15.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Ram Mandir, Raipur</center></h3>
            <p>Mahamaya Temple is temple dedicated to dual Goddess Lakshmi & Saraswati, located in Ratanpur and is one of the 52 Shakti Peethas, shrines of Shakti, the divine feminine, spread across India.Ratanpur is a small city, full of temples and ponds, situated around 25 km from district Bilaspur of Chhattisgarh.Goddess Mahamaya is also known as Kosaleswari,presiding deity of old Daksin Kosal region(modern Chhattisgarh state).</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Rani_Sati_Temple" target="_blank">
          <img src="img/t16.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Shree Rani Sati Mandir, Raipur</center></h3>
            <p>Rani Sati Temple is a temple located in Jhunjhunu, Jhunjhunu district, in the state of Rajasthan, India. It is the largest temple in India devoted to Rani Sati, a Rajasthani lady who lived sometime between the 13th and the 17th century and committed sati (self-immolation) on her husband's death. Various temples in Rajasthan and elsewhere are devoted to her worship and to commemorate her act. Rani Sati is also called Narayani Devi and referred to as Dadiji (grandmother).</p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="/w3images/lights.jpg" target="_blank">
          <img src="img/t17.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Shree Khatu Shayam Mandir</center></h3>
            <p>Danteshwari Temple is temple dedicated to Goddess Danteshwari, and is one of the 52 Shakti Peethas, shrines of Shakti, the divine feminine, spread across India. The temple built in the 14th century, is situated in Dantewada, a town situated 80 km from Jagdalpur Tehsil, Chhattisgarh. Dantewada is named after the Goddess Danteshwari, the presiding deity of the earlier Kakatiya rulers. Traditionally she is the Kuldevi (family goddess) of Bastar state.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="http://www.morraipur.in/type/custom/mahamaya-temple-raipur-chhattisgarh" target="_blank">
          <img src="img/t18.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>bhanpuri mahamaya mandir</center></h3>
            <p>Mahamaya Temple in Raipur is located in the old fort area of the city. it has been renovated many times and is visited by many devotees daily. The temple is dedicated to Devi Durga possessing all the good qualities and the power of Lord Vishnu and Lord Shiva. Besides daily pujas the two festivals of Navaratri and Durgapuja are celebrated here when thousands of devotees come here seeking blessings of Goddess Mahamaya.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Dudhadhari_temple" target="_blank">
          <img src="img/t19.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Dudhadhari Math</center></h3>
            <p>Dudhadhari temple is a Hindu temple, located at Raipur Mathpara in the Indian state of Chhattisgarh. The current Head Priest (Mahant) of the temple is Rajeshri Dr. Mahant Ramsundar Das Ji Maharaj. Sree Dudhdhari Math was founded by Sree Swami Balabhadradas Ji Maharaj, a monk belonging to 16th Century. As per legends Sree Swami Balabhadradas Ji Maharaj used to survive only on milk and hence came to be popularly known as Dudh Ahari Maharaj (one who consumes only milk). With the passage of time, for the sake of ease of pronunciation, the term evolved as Dudhdhari. The Math thus takes the name Dudhdhari Math.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Sirpur_Group_of_Monuments" target="_blank">
          <img src="img/t20.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Laxman Temple</center></h3>
            <p>The Lakshmana temple, also spelled Laxman temple, is a 7th-century brick temple, mostly damaged and ruined. The garbhagriya entrance along with the tower and door carvings of the Lakshmana temple at Sirpur are reasonably intact enough to be studied. Above the sanctum door's lintel are carvings show a reclining Vishnu on Sesha (Anantasayana Vishnu) and a panel on Krishna from Bhagavata Purana. Around the door are bands of carvings which show the ten avatars of Vishnu along with daily life and couples in various stages of courtship and mithuna. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://balodabazar.gov.in/tourist-place/giraudpuri-dham/" target="_blank">
          <img src="img/t21.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Girodhpuri Dham</center></h3>
            <p>Situated by the confluence of the Mahanadi and Jonk rivers, 40 KM from Balodabazar and 80 km from Bilaspur, the Giroudpuri Dham is one of Chhattisgarh’s most revered pilgrimage points. This tiny village, which has deep associations of spirituality and historical interest, is the birthplace of the founder of Chhattisgarh’s Satnami Panth, Guru Ghasidas. Born into a farmer family of the region, one day he rose up to become Guru Ghasidas, a much revered figure in Chhattisgarh.</p>
          </div>
        </a>
      </div>
    </div>
   
    </div>
  </div>
  
  

</div>

</body>
</html>


