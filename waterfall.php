<!DOCTYPE html>
<html lang="en">
<head>
  <title>Waterfall</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center>Most Popular Waterfalls</center></h2><br>

  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Amritdhara_falls" target="_blank">
          <img src="img/w1.jpg" alt="Lights" style="width:100%">
          <div class="caption">
            <h3><center>Amritdhara falls</center></h3>

            <p>Amrit Dhara is a natural waterfall located in Koriya district, state of Chhattisgarh, India. It originates from the Hasdeo River, which is a tributary of the Mahanadi River.[1] The fall is situated at a distance of 17 km from Chirmiri. The waterfall is ideally located on the Manendragarh-Baikunthpur road. The Amrit Dhara Waterfall in Koriya in Chhattisgarh in India falls from a height of 90.0 ft (27.4 metres). The waterfall is about 10.0–15.0 ft (3.0–4.6 metres) wide.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://balod.gov.in/tourism/place-of-interest/" target="_blank">
          <img src="img/w2.jpg" alt="Nature" style="width:100%">
          <div class="caption">
            <h3><center>Siya Devi Waterfall</center></h3>
            <p>SiyaDevi Temple is in Balod district of Chhattisgarh. This place is famous for Sita Maiya’s temple, situated in the heart of greenery of natural jungle. The temple  is very old and there is a Natural waterfall. It has been told that, Lord Rama has visited this place during his exile along with Laxman & Sita. The waterfall nearby it attracts many tourist which enhances its importance among the pilgrims of India. It’s one of the best tourist destinations in Chattisgarh.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://korba.gov.in/tourist-place/devapahari/" target="_blank">
          <img src="img/w3.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Devapahari</center></h3>
            <p>Devapahari is situated 58 KM. North East from Korba on the bank of Chornai river. This river made one lovely water fall named Govind Kunj in Devapahari.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Teerathgarh_Falls" target="_blank">
          <img src="img/w4.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Teerathgarh Falls</center></h3>
            <p>The Teerathgarh Falls is a block type waterfall on the Kanger River. It is located at a distance of 35 kilometres (22 mi) south-west of Jagdalpur. One can approach the falls from Darbha, near state highway that connects Jagdalpur to Sukma. One has to take a jeep at Darbha junction to visit Teerathgarh and Kutumsar. Kutumsar Caves and Kailash Gufa are nearby attractions. It is in Kanger Ghati National Park.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidayiq.com/Gullu-Waterfall-Jashpur-Sightseeing-900-16665.html" target="_blank">
          <img src="img/w5.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Gullu Waterfall </center></h3>
            <p>The Gullu Waterfall is located near Gullu village, around 25 km from Jashpur Nagar. It originates from Eb River. Badalkhol Abhyaran is close to this waterfall.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Hazara_Falls" target="_blank">
          <img src="img/w6.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Hazara Falls </center></h3>
            <p>Hazara Falls is 191 km from Nagpur, 33 km from Darekasa and close to the Gondia district. Hazara Falls, in Salekasa tehsil is a major tourist attraction during the rainy season. August to December is best time to go. It is located 1 km from Darekasa railway station. The place is also ideal for camping as well as trekking activities.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
     <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Chitrakote_Falls" target="_blank">
          <img src="img/w7.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Chitrakoot Falls</center></h3>
            <p>The Chitrakoot Falls is located on the Indravati River. The river originates in the kalahandi region of Odisha, in the Vindhya Range of hills, flows westward and then forms a fall at Chitrakoote, enters Telangana and finally flows into the Godavari River, after traversing 240 miles (390 km) in the state, at Bhadrakali. The free drop of the falls is a sheer height of about 30 metres (98 ft). Because of its horseshoe shape, it is compared with the Niagara Falls and is given in the sobriquet ‘the Small Niagara Falls’. During the rainy season, from July and October, rainbows are created with sun rays reflecting on mist from the waterfall.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Dudhsagar_Falls" target="_blank">
          <img src="img/w8.webp" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Doodh Dhara</center></h3>
            <p>Doodh Dhara Falls (literally Sea of Milk) is a four-tiered waterfall located on the Mandovi River in the Indian state of Goa. It is 60 km from Panaji by road and is located on the Madgaon-Belagavi rail route about 46 km east of Madgaon and 80 km south of Belagavi. Dudhsagar Falls is amongst India's tallest waterfalls with a height of 310 m (1017 feet) and an average width of 30 metres (100 feet).</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.indianholiday.com/tourist-attraction/jashpur/rivers-caves-waterfalls-in-jashpur/rajpuri-waterfall.html" target="_blank">
          <img src="img/w9.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Rajpuri Waterfall  </center></h3>
            <p>Jashpur situated on the Northern part of Chhattisgarh, India is perfect for a leisurely weekend getaway. The Uraon tribal population and their small settlements lend a different charm to Jashpur. These tribals have their own folklore and tradition. There is nothing like watching a tribal dance on a moonlit night. Jashpur is replete with hilly terrain and forests in the Upper Ghats while the lower plain consists of sprawling Rivers and waterfalls. The Rivers, Caves and Waterfalls in Jashpur provides ample opportunities for angling, trekking and hiking. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
   <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://bastar.gov.in/en/tourist-place/chitradhara-waterfall/" target="_blank">
          <img src="img/w10.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Chitradhara waterfall</center></h3>
            <p>Bastar has a series of waterfalls, many of them are perennials, while some are deprived of water during the summer, but during the rainy and winter season their beauty is unique. Chitradhara comes in the latter category.Chitradhara waterfall is an amazing example of Bastar’s beauty. On the way to Chitrakote Falls, a small river flows through the valley of a small hill and it is the beginning of the river bed of the farmers. For this reason, there is water during the rain, but in the summer, its excellence becomes faded.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Mendri_Ghumar" target="_blank">
          <img src="img/w11.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Mendri Ghumar Waterfall</center></h3>
            <p>Mendri Ghumar Waterfall is located in the middle of Chitrakote Falls, Tirtha and Barasur and situated in Jagdalpur. The height of the fall is almost 70 meters with a lush green surrounding and eye catchy places. The interesting part is that the Mendri Ghumar Waterfall forms its best during the rainy seasons. The water collected from the rains, continuously flows through the hill thus making it an eye catchy waterfall. It is surrounded by lush green forest on both its sides, making it one of the most wanted ecotourism destination in the state.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.indianholiday.com/tourist-attraction/jashpur/rivers-caves-waterfalls-in-jashpur/rani-dah-waterfall.html" target="_blank">
          <img src="img/w12.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Rani Dah Waterfall</center></h3>
            <p>
Jashpur located on the Northern part of Chhattisgarh is inhabited mainly by tribal populations who have their own culture, folklore and tradition. The Rivers, Caves and Waterfalls in Jashpur are ideal for trekking and hiking. There are ample Tourist Attractions in Jashpur. Moreover most of the waterfalls are situated in close proximity to each other so you can complete your tour to these places at one go. Rani Dah Waterfall, Jashpur is an ideal place for a quiet picnic, away from the din and bustle of city life. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.pilgrimaide.com/hatkeshwar-mahadev-temple,-raipur.html" target="_blank">
          <img src="img/w13.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Aapkhol Waterfalls</center></h3>
            <p>Located 5 kms from Raipur on the banks of River Kharun, Chhattisgarh, Hatkeshwar Mahadev Temple is one of the most sacred pilgrimage destinations in central India dedicated to Lord Shiva worshipped as Lord Hatakeshwar. The temple was built by Hajiraj Naik during the rule of Brahmadeo Rai, son of King Ramachandra of Kalchuri dynasty in 1402.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.holidify.com/places/raipur/ghatarani-waterfalls-sightseeing-1254225.html" target="_blank">
          <img src="img/w14.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Ghatarani Waterfalls</center></h3>
            <p>Located 85 km from the city of Raipur is Ghatarani Waterfalls , the largest in the state of Chattisgarh. Surrounded by lush greenery, this breathtaking sight is a popular picnic spot. Ghatarani Waterfalls is a favourite picnic spot to go to on the weekend along with your friends and family. A small trek through a forest would lead you to this beautiful natural beauty. At the bottom of the waterfall lies a naturally formed pool. </p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://www.indianholiday.com/tourist-attraction/kanker/mountains-waterfalls-in-kanker/charre-marre-waterfall.html" target="_blank">
          <img src="img/w15.png" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Charre-Marre Waterfall</center></h3>
            <p>Charre-Marre Waterfall, Kanker is a zig zag waterfall on the Jogi river. Kanker District is located in the southern region of Chhattisgarh. It was earlier a part of the old Bastar district. There are five rivers that flow through the district. The five rivers are Mahanadi River, Turu River, Sindur River, Doodh River and Hatkul River. The district's economy is based on agriculture and the main crop of the area is Rice. The other important crops of the area are sugar cane, bhutta, chana, Kodo, Moong, Tilli and wheat. </p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://raigarh.gov.in/en/tourist-place/ram-jharna/" target="_blank">
          <img src="img/w16.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center> Ram Jharna</center></h3>
            <p>It is about 18 Kms. from the District Head quarters. It has a natural water source. According to History, Lord Ram once had been here during his Vanwas, and drank water from the water source. Hence the name Ram Jharna. It is a very good picnic spot.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://kanker.gov.in/tourist-place/malanjhkudum-waterfall/" target="_blank">
          <img src="img/w17.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Malanjh kudum</center></h3>
            <p>Towards South direction and 15 kilometers away form Kanker, there is a small mountain. On this mountain there is a spot named Neele Gondi from where the doodh river takes its shape. After crossing the 10 kilometers length of mountaineer path there is a place named as Malanjhkudum from where the river produces three water falls, the heights of these water falls are 10 meters. 15 meters and 9 meters respectively. The slope of this water fall is like a leader.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Rakasganda" target="_blank">
          <img src="img/w18.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Rakasganda Fall</center></h3>
            <p>The Rakasganda Fall is a tourist spot situated in Balrampur district, Chhattisgarh, India. This fall is on Rihand River, and it is around 150.0 km (93.2 mi) from Ambikapur and around 60.0 km (37.3 mi) from Wadraf Nagar, which is a small city and tehsil[1] located amidst dense forest. Many transport buses are available, from Ambikapur and Wadraf Nagar, to reach Balangi, the last village before Rakasganda. From Balangi four-wheelers or two-wheelers can be hired to reach Rakasganda. The best time to see Rakasganda is around April – June.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://korba.gov.in/tourist-place/kendai/" target="_blank">
          <img src="img/w19.jpg" alt="Lights" style="width:100%">
          <div class="caption">
             <h3><center>Kendai</center></h3>
            <p>Kendai is a village situated at a distance 85 km from korba district headquarter on the Bilaspur-Ambikapur State highway No. 5. This is one of the beautiful picnic spot in the district. There is one lovely waterfall having 75 feet height.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://rajnandgaon.nic.in/tourist-place/kharkhara-dam/" target="_blank">
          <img src="img/w20.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Kharkhara Dam</center></h3>
            <p>The total driving Distance from Rajnandgaon to Kharkhara Dam is 99.0 Kms Or 61.515729 Miles . Your trip begins at Rajnandgaon. It ends at Kharkhara Dam. If you are planning a road trip, it will take you 0 Days : 2 Hours : 39 Minutes, To travel from Rajnandgaon to Kharkhara Dam.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://en.wikipedia.org/wiki/Dudhadhari_temple" target="_blank">
          <img src="img/w21.jpg" alt="Fjords" style="width:100%">
          <div class="caption">
             <h3><center>Dudhadhari Math</center></h3>
            <p>Dudhadhari temple is a Hindu temple, located at Raipur Mathpara in the Indian state of Chhattisgarh. The current Head Priest (Mahant) of the temple is Rajeshri Dr. Mahant Ramsundar Das Ji Maharaj. Sree Dudhdhari Math was founded by Sree Swami Balabhadradas Ji Maharaj, a monk belonging to 16th Century. As per legends Sree Swami Balabhadradas Ji Maharaj used to survive only on milk and hence came to be popularly known as Dudh Ahari Maharaj (one who consumes only milk). With the passage of time, for the sake of ease of pronunciation, the term evolved as Dudhdhari. The Math thus takes the name Dudhdhari Math.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
   <div class="row">
   
    <div class="col-md-4">
      <div class="thumbnail">
        <a href="https://balodabazar.gov.in/tourist-place/giraudpuri-dham/" target="_blank">
          <img src="img/w23.jpg" alt="Nature" style="width:100%">
          <div class="caption">
             <h3><center>Gaurghat Waterfall</center></h3>
            <p>This waterfall situated on Hasdeo river that is away 33 kilometers from the district headquarters of Korea.</p>
          </div>
        </a>
      </div>
    </div>
   
    </div>
  </div>
  
  

</div>

</body>
</html>


